{
  const {
    html,
  } = Polymer;
  /**
    `<cells-diego-login>` Description.

    Example:

    ```html
    <cells-diego-login></cells-diego-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-diego-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsDiegoLogin extends Polymer.Element {

    static get is() {
      return 'cells-diego-login';
    }

    static get properties() {
      return {
        prop1: {
            type: String,
            value: 'login-diego'
          },
          mail:{
            type: String
          },
          psw: {
            type: String
          },

          iniciado: {
            type: Boolean, 
            notify: true
          },
      };
    }

    inic() {
    
        if(this.mail=="diego" && this.psw=="yndiego"){
          this.set("iniciado",true)
        }else{
          this.set("iniciado",false)
        }
      }
    static get template() {
      return html `
      <style include="cells-diego-login-styles cells-diego-login-shared-styles"></style>
      <slot></slot>
      
          <p>Welcome to Cells</p>

          <H2>LOGIN</H2>
      <input value="{{user::input}}" type="text" placeholder="Email" class="input">
      <input value="{{pass::input}}" type="password" placeholder="Password" class="input">
      <button type="button" on-click="inic" class="bot">Enter</button> 


      
      `;
    }
  }

  customElements.define(CellsDiegoLogin.is, CellsDiegoLogin);
}